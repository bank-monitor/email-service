package cloud.accountmonitor.emailservice.controller;

import cloud.accountmonitor.emailservice.dto.MailMessageDto;
import cloud.accountmonitor.emailservice.service.MailReaderService;
import cloud.accountmonitor.emailservice.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(path = "/")
public class ReadEmailController {

    @Autowired
    private SchedulerService readerService;

    @GetMapping
    public Map<LocalDateTime, List<String>> readMessages(){
        return this.readerService.readMail();
    }
}
