package cloud.accountmonitor.emailservice.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class MailMessageDto {

    private String fromEmail;
    private String fromName;
    private String subject;
    private String html;
    private String text;
    private LocalDateTime date;
}
