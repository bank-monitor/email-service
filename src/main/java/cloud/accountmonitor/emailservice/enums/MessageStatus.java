package cloud.accountmonitor.emailservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageStatus {

  ANSWERED_BIT(1), DELETED_BIT(2),DRAFT_BIT(4),
  FLAGGED_BIT(8), RECENT_BIT(16),SEEN_BIT(32);

  private int number;
}
