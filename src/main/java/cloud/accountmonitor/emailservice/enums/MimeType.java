package cloud.accountmonitor.emailservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MimeType {

    TEXT("text/plain"), HTML("text/html");

    private String type;
}
