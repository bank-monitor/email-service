package cloud.accountmonitor.emailservice.service;

import cloud.accountmonitor.emailservice.dto.MailMessageDto;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class SchedulerService {

  @Autowired
  private MailReaderService mailReaderService;

  @Scheduled(cron = "0 0 */4 * * *")
  public Map<LocalDateTime, List<String>> readMail() {
    return mailReaderService.getMessages()
        .stream()
        .filter(message -> "mailing@zumail.cz".equalsIgnoreCase(message.getFromEmail()))
        .filter(dto -> Objects.nonNull(dto.getHtml()))
        .collect(Collectors.toMap(MailMessageDto::getDate, dto -> this.processEmail(dto.getHtml())));
  }

  private List<String> processEmail(String text) {
    log.info("process email with text {}", text);
    Document html = Jsoup.parse(text);
    Elements select = html.select("a[href]");
    List<String> href = select.stream()
        .map(Element::attributes)
        .filter(attributes -> attributes.hasKeyIgnoreCase("href"))
        .map(attr -> attr.get("href"))
        .collect(Collectors.toList());

    return href;
  }
}
