package cloud.accountmonitor.emailservice.service;

import cloud.accountmonitor.emailservice.dto.MailMessageDto;
import cloud.accountmonitor.emailservice.enums.MimeType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static cloud.accountmonitor.emailservice.enums.MimeType.HTML;
import static cloud.accountmonitor.emailservice.enums.MimeType.TEXT;

@Slf4j
@Service
public class MailReaderService {

    private static final String HOST = "mail.imap.host";
    private static final String USER = "mail.imap.user";
    private static final String PASS = "mail.imap.password";

    private Properties getEmailProperties() {
        Properties properties = new Properties();
        properties.put(HOST, "imap.centrum.cz");
//        properties.put("mail.imap.port", "993");
//        properties.put("mail.imap.starttls.enable", "true");
        properties.put(USER, "petr.vich@centrum.cz");
        properties.put(PASS, "And1l3k2");
        return properties;
    }

    public List<MailMessageDto> getMessages() {
        Properties emailProperties = getEmailProperties();
        Session emailSession = Session.getDefaultInstance(emailProperties);
        Store store = null;
        Folder emailFolder = null;
        try {
            store = emailSession.getStore("imaps");
            store.connect(emailProperties.getProperty(HOST), emailProperties.getProperty(USER), emailProperties.getProperty(PASS));
            emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);
            int unreadMessageCount = emailFolder.getUnreadMessageCount();
            int messageCount = emailFolder.getMessageCount();
            return Arrays.asList(emailFolder.getMessages()).stream()
                    .map(this::toDto)
                    .collect(Collectors.toList());
        } catch (NoSuchProviderException e) {
            log.error("No such email provider.", e);
            throw new RuntimeException(e);
        } catch (MessagingException e) {
            log.error("MessagingException.", e);
            throw new RuntimeException(e);
        } finally {
            try {
                emailFolder.close(false);
                store.close();
            } catch (MessagingException e) {
                log.error("MessagingException.", e);
            }
        }
    }

    private MailMessageDto toDto(Message message) {
        try {
            InternetAddress from = (InternetAddress) message.getFrom()[0];
            Map<MimeType, String> map = getTextFromMessage(message);
            return MailMessageDto.builder()
                    .subject(message.getSubject())
                    .html(map.get(HTML))
                    .text(map.get(TEXT))
                    .date(LocalDateTime.ofInstant(message.getReceivedDate().toInstant(), ZoneId.systemDefault()))
                    .fromEmail(from.getAddress())
                    .fromName(from.getPersonal())
                    .build();
        } catch (MessagingException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<MimeType, String> getTextFromMessage(Message message) throws MessagingException, IOException {
        if (message.isMimeType(TEXT.getType())) {
            Map<MimeType, String> map = new HashMap<>();
            map.put(TEXT, message.getContent().toString());
            return map;
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            return getTextFromMimeMultipart(mimeMultipart);
        }
        throw new RuntimeException("Unsupported Mime type for value " + message.getContentType());
    }

    private Map<MimeType, String> getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        Map<MimeType, String> result = new HashMap<>();
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType(TEXT.getType())) {
                String part = "\n" + bodyPart.getContent();
                this.addToMap(result, TEXT, part);
            } else if (bodyPart.isMimeType(HTML.getType())) {
                String html = (String) bodyPart.getContent();
                this.addToMap(result, HTML, html);
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                Map<MimeType, String> map = getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
                this.addToMap(result, TEXT, map.get(TEXT));
                this.addToMap(result, HTML, map.get(HTML));
            }
        }
        return result;
    }

    private void addToMap(Map<MimeType, String> map, MimeType key, String value) {
        String val;
        if (map.containsKey(key)) {
            val = map.get(key) + value;
        } else {
            val = value;
        }
        map.put(key, val);
    }
}
