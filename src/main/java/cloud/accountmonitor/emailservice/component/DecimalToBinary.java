package cloud.accountmonitor.emailservice.component;

import org.springframework.stereotype.Component;

@Component
public class DecimalToBinary {

  public void printBinaryFormat(int number) {

    if (number > 63) {
      throw new RuntimeException("The number " + number + " is higher than allowed");
    }
    int binary[] = new int[6];
    int index = 0;
    while (number > 0) {
      binary[index++] = number % 2;
      number = number / 2;
    }

  }
}
